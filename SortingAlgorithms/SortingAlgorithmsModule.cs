using Autofac;

namespace SortingAlgorithms
{
    public class SortingAlgorithmsModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<BubbleSort>().Keyed<ISortingAlgorithm>(SortingAlgorithmType.Bubble);
        }
    }
}