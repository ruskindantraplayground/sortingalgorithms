using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace SortingAlgorithms
{
    public interface ISortingAlgorithm
    {
        IEnumerable<T> Sort<T>([NotNull] IEnumerable<T> items)  where T : IComparable<T>;
    }
}
