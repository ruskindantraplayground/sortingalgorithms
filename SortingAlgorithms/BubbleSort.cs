using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections;
using Microsoft.Extensions.Logging;

namespace SortingAlgorithms
{
    internal class BubbleSort : SortBase
    {
        public BubbleSort(ILogger<BubbleSort> logger) : base(logger)
        {
        }
        
        public override IEnumerable<T> Sort<T>(IEnumerable<T> items)
        {
            Logger.LogInformation("Sorting started using strategy {Name}", nameof(BubbleSort));
            
            var collectionToBeSorted = items.ToArray();
            for (int i = 0; i < collectionToBeSorted.Length; i++)
            {
                for (int j = 0; j < collectionToBeSorted.Length - 1; j++)
                {
                    if (collectionToBeSorted[j].CompareTo(collectionToBeSorted[j+1]) == 1)
                    {
                        // swap the elements
                        T temp = collectionToBeSorted[j];
                        collectionToBeSorted[j] = collectionToBeSorted[j+1];
                        collectionToBeSorted[j+1] = temp;
                    }
                }
            }
            return collectionToBeSorted;
        }
    }
}
