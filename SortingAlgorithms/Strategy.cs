using System;
using Autofac.Features.Indexed;

namespace SortingAlgorithms
{
    public class Strategy<T> where T : IComparable<T>
    {
        private readonly IIndex<SortingAlgorithmType, ISortingAlgorithm> _algorithms;

        public Strategy(IIndex<SortingAlgorithmType, ISortingAlgorithm> algorithms)
        {
            _algorithms = algorithms;
        }

        public ISortingAlgorithm Get(SortingAlgorithmType sortingAlgorithmType)
        {
            return _algorithms[sortingAlgorithmType];
        }
    }
}