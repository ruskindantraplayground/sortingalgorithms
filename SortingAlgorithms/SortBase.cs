using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace SortingAlgorithms
{
    internal abstract class SortBase : ISortingAlgorithm
    {
        protected readonly ILogger Logger;

        protected SortBase(ILogger logger)
        {
            Logger = logger;
        }
        
        public abstract IEnumerable<T> Sort<T>(IEnumerable<T> items) where T : IComparable<T>;
    }
}