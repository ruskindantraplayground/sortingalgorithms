using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using Autofac.Features.Indexed;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Diagnosers;
using BenchmarkDotNet.Environments;
using BenchmarkDotNet.Exporters;
using BenchmarkDotNet.Jobs;
using BenchmarkDotNet.Running;
using BenchmarkDotNet.Toolchains.CsProj;
using BenchmarkDotNet.Toolchains.DotNetCli;
using Microsoft.Extensions.Logging;

namespace SortingAlgorithms.Runner
{
    internal class PerformanceMeasures : IPerformanceMeasures
    {
        private readonly IIndex<SortingAlgorithmType, ISortingAlgorithm> _sortingAlgorithms;

        public PerformanceMeasures(ILogger<PerformanceMeasures> logger, IIndex<SortingAlgorithmType, ISortingAlgorithm> sortingAlgorithms)
        {
            _sortingAlgorithms = sortingAlgorithms;
        }

        public Task StartAsync()
        {
            var bubbleSort = _sortingAlgorithms[SortingAlgorithmType.Bubble];
            var sorted = bubbleSort.Sort(new[] {8, 6, 4, 9, 0, 2, 1, 3});

            var summary = BenchmarkRunner.Run<Md5VsSha256>(new BenchmarkConfig());
            //var summary = BenchmarkRunner.Run<AlgorithmBenchmark>();
            
            Console.WriteLine(summary);

            return Task.CompletedTask;
        }
    }

    [CoreJob]
    [RPlotExporter, RankColumn]
    public class AlgorithmBenchmark
    {
        [Params(1000, 10000)]
        public int N;

        [Benchmark]
        public IEnumerable<int> Measure()
        {
            Thread.Sleep(1000);
            return null;
        }
    }
    
    [CoreJob]
    [RPlotExporter, RankColumn]
    public class Md5VsSha256
    {
        private SHA256 sha256 = SHA256.Create();
        private MD5 md5 = MD5.Create();
        private byte[] data;

        [Params(1000, 10000)]
        public int N;

        [GlobalSetup]
        public void Setup()
        {
            data = new byte[N];
            new Random(42).NextBytes(data);
        }

        [Benchmark]
        public byte[] Sha256() => sha256.ComputeHash(data);

        [Benchmark]
        public byte[] Md5() => md5.ComputeHash(data);
    }
    
    public class BenchmarkConfig : ManualConfig
    {
        public BenchmarkConfig()
        {
            Add(Job.Default.With(Runtime.Core).With(CsProjCoreToolchain.From(NetCoreAppSettings.NetCoreApp20)).AsBaseline().WithId("2.0"));
            Add(Job.Default.With(Runtime.Mono).WithId("Mono"));

            Add(MemoryDiagnoser.Default);

            Add(DefaultConfig.Instance.GetValidators().ToArray());
            Add(DefaultConfig.Instance.GetLoggers().ToArray());
            Add(DefaultConfig.Instance.GetColumnProviders().ToArray());

            Add(MarkdownExporter.GitHub);
        }
    }
}