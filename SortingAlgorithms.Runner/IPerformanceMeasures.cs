using System.Threading.Tasks;

namespace SortingAlgorithms.Runner
{
    internal interface IPerformanceMeasures
    {
        Task StartAsync();
    }
}