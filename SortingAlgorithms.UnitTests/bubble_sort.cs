using System.Collections.Generic;
using Autofac;
using AutoFixture;
using FluentAssertions;
using Xunit;

namespace SortingAlgorithms.UnitTests
{
    public class bubble_sort : TestBase
    {
        [Fact]
        public void should_sort_hardcoded_array()
        {
            var fixture = new Fixture();

            IEnumerable<int> randomIntArray = new[] {10, 3, 5, 9, 7, 6};

            var sut = Container.ResolveKeyed<ISortingAlgorithm>(SortingAlgorithmType.Bubble);
            var result = sut.Sort(randomIntArray);

            result.Should().BeInAscendingOrder();

        }
    }
}