using Autofac;
using Microsoft.Extensions.Logging;

namespace SortingAlgorithms.UnitTests
{
    public abstract class TestBase
    {
        protected readonly IContainer Container;
        
        protected TestBase()
        {
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterInstance(new LoggerFactory()).As<ILoggerFactory>();
            containerBuilder.RegisterGeneric(typeof(Logger<>)).As(typeof(ILogger<>)).SingleInstance();
            containerBuilder.RegisterModule<SortingAlgorithmsModule>();
            Container = containerBuilder.Build();
        }
    }
}